package solution_02;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import utils.Main;

import java.util.concurrent.ThreadPoolExecutor;

import static org.junit.jupiter.api.Assertions.*;

class ISequenceTest {

    private static final String THREAD_SAFE_BEAN_NAME = "threadSafeDefault";
    private static final String NOT_THREAD_SAFE_BEAN_NAME = "notThreadSafeDefault";

    /**
     * Количество потоков при работе теста
     */
    private static final int COUNT_THREAD = 10;

    /**
     * ожидаем получить число приблизительно равное
     */
    private static final int CAPACITY = 1000000;

    /**
     * расчитываем количество увеличения счетчика для каждого потока
     */
    private static final int COUNT_IMPL = CAPACITY / COUNT_THREAD;

    /**
     * ожидаемое точное число
     */
    private static final int TRUE_CAPACITY = COUNT_IMPL * COUNT_THREAD;

    /**
     * потокобезопасная реализация
     */
    private ISequence threadSafeInstance;

    /**
     * непотокобезопасная реализация
     */
    private ISequence notThreadSafeInstance;

    @BeforeEach
    void setUp() {
        threadSafeInstance = Main.getSequence(THREAD_SAFE_BEAN_NAME);
        notThreadSafeInstance = Main.getSequence(NOT_THREAD_SAFE_BEAN_NAME);
    }

    /**тест потокобезопасного sequence*/
    @Test
    void threadSafe() {
        ISequence sequence = threadSafeInstance;
        testThreadSafety(sequence, COUNT_THREAD);
        assertEquals(TRUE_CAPACITY, sequence.curval().intValue());
    }

    /**тест непотокобезопасного sequence*/
    @Test
    void notThreadSafe() {
        ISequence sequence = notThreadSafeInstance;
        testThreadSafety(sequence, COUNT_THREAD);
        assertEquals(TRUE_CAPACITY, sequence.curval().intValue());
    }


    /**
     * тело теста на потокобезопасность
     */
    void testThreadSafety(ISequence sequence, int countThread) {
        Thread thread = null;
        for (int i = 0; i < countThread; i++) {
            thread = runThread(sequence);
            thread.start();
        }

        try {
            /*цепляемся к последнему запущеному потоку*/
            thread.join();
            /*чтобы не мудрить с синхронизацией потоков, подождем пока точно завершится работа всех потоков*/
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    /**
     * запускает поток с установленным {@linkplain ISequenceTest#COUNT_IMPL количеством}
     * итераций для полученного {@linkplain ISequence счетчика}
     */
    private Thread runThread(ISequence sequence) {
        return new Thread(() -> {
            for (int i = 0; i < COUNT_IMPL; i++) {
                sequence.next();
            }
        });
    }

    /**тест непотокобезопасного sequence бина*/
    @Test
    void testNotThreadSafeBeans() throws InterruptedException {
        String beanName = "notThreadSafeDefaultBeanTest";
        Thread thread = null;
        for (int i = 0; i < COUNT_THREAD; i++) {
            ISequence bean = Main.getSequence(beanName);
            thread = runThread(bean);
            thread.start();
        }
        thread.join();
        Thread.sleep(1000);
        assertEquals(TRUE_CAPACITY, Main.getSequence(beanName).curval().intValue());
    }

    /**тест потокобезопасного sequence бина*/
    @Test
    void testThreadSafeBeans() throws InterruptedException {
        String beanName = "threadSafeDefaultBeanTest";
        Thread thread = null;
        for (int i = 0; i < COUNT_THREAD; i++) {
            ISequence bean = Main.getSequence(beanName);
            thread = runThread(bean);
            thread.start();
        }
        thread.join();
        Thread.sleep(1000);
        assertEquals(TRUE_CAPACITY, Main.getSequence(beanName).curval().intValue());
    }

}