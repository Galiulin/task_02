package Solution_04;

import org.junit.jupiter.api.Test;
import utils.Main;

import static org.junit.jupiter.api.Assertions.*;

public class Solution04Test {

    private static final String PROT_NAME = "notThreadSafePrototype";
    private static final String SING_NAME = "notThreadSafeSingleton";

    @Test
    void testPrototype() {
        /*пояснение как это работает: в xml кофигурации при инициализации бина
        * в value устанавливается число. Это число помогает устанавливать исходное рандомное значение
        * в sequence. Соответственно, каждый раз при получении бина prototype число будет разным*/
        assertNotEquals(Main.getSequence(PROT_NAME), Main.getSequence(PROT_NAME));
        assertNotEquals(Main.getSequence(PROT_NAME), Main.getSequence(PROT_NAME));
        assertNotEquals(Main.getSequence(PROT_NAME), Main.getSequence(PROT_NAME));
    }

    @Test
    void testSingleton() {
        /*здесь полностью идентичный код, но скоуп отличается.*/
        assertEquals(Main.getSequence(SING_NAME), Main.getSequence(SING_NAME));
        assertEquals(Main.getSequence(SING_NAME), Main.getSequence(SING_NAME));
        assertEquals(Main.getSequence(SING_NAME), Main.getSequence(SING_NAME));
    }
}
