package solution_01;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class CacheTest {

    private Cache cache;

    @BeforeEach
    void setUp() throws IOException {
        this.cache = new Cache(new File("src/main/test/java/dataTest/keys_values.txt"), 30);
    }

    @Test
    void printCache() {
        cache.printCache();
    }


    @AfterEach
    void tearDown() {
    }
}