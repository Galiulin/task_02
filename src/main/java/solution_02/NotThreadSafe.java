package solution_02;

import org.springframework.stereotype.Component;

import java.math.BigInteger;

/**Не потокобезопасная реализация*/
@Component
public class NotThreadSafe implements ISequence {
    private BigInteger value;

    public NotThreadSafe(){
        this.value = BigInteger.valueOf(0);
    }

    public NotThreadSafe(BigInteger initValue) {
        this.value = initValue;
    }

    public void setValue(int value) {
        /*fixme т.к. это число используется для дальнейшего сравнения различия в бинах скоупа
        * prototype, при достаточном количестве повторений есть вероятность выдачи одинаковых значений*/
        BigInteger newValue = BigInteger.valueOf((long) (Math.random() * value));
        this.value = newValue;
    }

    @Override
    public BigInteger next() {
        return value = value.add(BigInteger.valueOf(1));
    }

    @Override
    public BigInteger curval() {
        return value;
    }
}
