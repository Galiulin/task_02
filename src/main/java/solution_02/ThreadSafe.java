package solution_02;

import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.util.concurrent.atomic.AtomicReference;

@Component
/**Потокобезопасная реализация*/
public class ThreadSafe implements ISequence {

    private AtomicReference<BigInteger> value;

    public ThreadSafe(){
        this.value = new AtomicReference<>(BigInteger.ZERO);
    }

    public ThreadSafe(BigInteger initValue) {
        this.value = new AtomicReference<>(initValue);
    }

    public void setValue(AtomicReference<BigInteger> value) {
        this.value = value;
    }

    @Override
    public BigInteger next() {
        return value.updateAndGet(value -> value.add(BigInteger.ONE));
    }

    @Override
    public BigInteger curval() {
        return value.get();
    }
}
