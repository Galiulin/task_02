package solution_02;

import java.math.BigInteger;

public interface ISequence {
    BigInteger next();
    BigInteger curval();
}
