package solution_01;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class Cache {
    private final ReadWriteLock lock;
    private final File file;
    private Map<String, String> cache;
    private final int howOften;
    private AtomicInteger countUsage;

    public Cache(File file, int howOften) throws IOException {
        if (!file.exists()) {
            throw new IllegalArgumentException("файл не существует");
        } else if (!file.isFile()) {
            throw new IllegalArgumentException("ссылка не на файл");
        } else if (!file.canRead()) {
            throw new IllegalArgumentException("файл невозможно прочесть");
        }
        this.lock = new ReentrantReadWriteLock();
        this.file = file;
        this.cache = new HashMap<>();
        this.howOften = howOften;
        this.countUsage = new AtomicInteger(0);
        readFile();
    }

    private void readFile() throws IOException {
        lock.writeLock().lock();//нужно дождаться обновления кеша, поэтому блокировка здесь а не позже
        Map<String, String> cache = new HashMap<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            while (reader.ready()) {
                String line = reader.readLine();
                String[] split = line.split("-");
                if (split.length > 1) {
                    cache.put(split[0], split[1]);
                }
            }
            this.cache = cache;
        } finally {
            lock.writeLock().unlock();
        }
    }

    public String getValue(String key) throws IOException {
        if (countUsage.incrementAndGet() == howOften) {
            readFile();
        }
        lock.readLock().lock();
        String value = cache.get(key);
        lock.readLock().unlock();
        return value;
    }

    public void printCache() {
        lock.readLock().lock();
        cache.forEach((key, value) -> System.out.println(key + "=" + value));
        lock.readLock().unlock();
    }

}
