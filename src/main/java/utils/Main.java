package utils;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import solution_02.ISequence;

public class Main {

    /*может стоит переписать?....*/
    private static final ApplicationContext CONTEXT = new ClassPathXmlApplicationContext("applicationContext.xml");

    public static ISequence getSequence(String sequenceBeanName) {
        return (ISequence) CONTEXT.getBean(sequenceBeanName);
    }
}
